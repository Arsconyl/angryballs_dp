package angryballs.vues;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.Vector;

import angryballs.modele.Bille.Bille;
import angryballs.outilsvues.Dessinateur.VisiteurDessinBille;

import javax.swing.*;


/**
 * responsable du dessin des billes
 * 
 *  ICI : IL N'Y A RIEN A CHANGER 
 *  
 * 
 * */
public class Billard extends Canvas
{
    boolean repaintInProgress = false;

    private Vector<Bille> billes;
    private Chrono chrono;

    Billard(Vector<Bille> billes)
    {
        this.billes = billes;
        setIgnoreRepaint(true);
        this.chrono = new Chrono(this);
        //new Timer(16, chrono).start();
        //this.createBufferStrategy(2);
    }
    /* (non-Javadoc)
     * @see java.awt.Canvas#paint(java.awt.Graphics)
     */
    public void paint(Graphics graphics)
    {
        int i;

        for ( i = 0; i < this.billes.size(); ++i)
            //this.billes.get(i).dessine(graphics);
            this.billes.get(i).accept(new VisiteurDessinBille(graphics));

        //System.out.println("billes dans le billard = " + billes);
    }


    @Override
    public void update(Graphics graphics) {
        Graphics offgc;
        Image offscreen;
        //Dimension d = size();
        Dimension d = new Dimension(this.getWidth(), this.getHeight());

        // create the offscreen buffer and associated Graphics
        offscreen = createImage(d.width, d.height);
        offgc = offscreen.getGraphics();
        // clear the exposed area
        offgc.setColor(getBackground());
        offgc.fillRect(0, 0, d.width, d.height);
        offgc.setColor(getForeground());
        // do normal redraw
        paint(offgc);
        // transfer offscreen to window
        graphics.drawImage(offscreen, 0, 0, this);
    }

    public void myRepaint()
    {
        // wasting too much time doing the repaint... ignore it
        if(repaintInProgress)
            return;

        // so I won't be called 2 times in a row for nothing
        repaintInProgress = true;

        BufferStrategy strategy= getBufferStrategy();
        Graphics graphics = strategy.getDrawGraphics();

        graphics.setColor(getBackground());

        graphics.fillRect(0, 0, this.getSize().width, this.getSize().height);
        graphics.setColor(getForeground());


        for ( int i = 0; i < this.billes.size(); ++i)
            //this.billes.get(i).dessine(graphics);

            this.billes.get(i).accept(new VisiteurDessinBille(graphics));

        if(graphics != null)
            graphics.dispose();

        // show next buffer
        strategy.show();

        // synchronized the blitter page shown
        Toolkit.getDefaultToolkit().sync();

        repaintInProgress = false;



    }

    public void init()
    {
        this.createBufferStrategy(2);
        new Timer(16, this.chrono).start();
    }

}
