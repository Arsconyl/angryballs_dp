package angryballs.vues.ObserveurEvent;

import angryballs.AnimationBilles;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 *  ICI : IL N'Y A RIEN A CHANGER 
 *  
 *  */

public class EcouteurBoutonArreter implements Observateur
{
    AnimationBilles animationBilles;

    public EcouteurBoutonArreter(AnimationBilles animationBilles)
    {
        this.animationBilles = animationBilles;
    }

    @Override
    public void actualiser(Observable observable) {

        System.out.println("clic sur le bouton d'arrêt ! ");
        this.animationBilles.arréterAnimation();
    }
}
