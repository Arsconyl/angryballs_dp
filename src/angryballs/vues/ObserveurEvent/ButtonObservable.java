package angryballs.vues.ObserveurEvent;

import angryballs.AnimationBilles;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ButtonObservable extends Button implements Observable{
    private Button b;
    private ArrayList<Observateur> observateurs;

    public ButtonObservable(String label)
    {
        super(label);
        observateurs = new ArrayList<>();
        this.addActionListener(actionEvent -> notifyObservers());
    }

    @Override
    public void addObservateur(Observateur observateur) {
        observateurs.add(observateur);
    }

    @Override
    public void notifyObservers() {
        for(Observateur observateur : observateurs){
            observateur.actualiser(this);
        }
    }
}
