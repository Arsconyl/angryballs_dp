package angryballs.vues.ObserveurEvent;

import angryballs.AnimationBilles;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 *  ICI : IL N'Y A RIEN A CHANGER 
 *  
 *  */

public class EcouteurBoutonLancer implements Observateur
{
    AnimationBilles animationBilles;

    public EcouteurBoutonLancer(AnimationBilles animationBilles)
    {
        this.animationBilles = animationBilles;
    }

    @Override
    public void actualiser(Observable observable) {

        System.out.println("clic sur le bouton de lancer ! ");
        this.animationBilles.lancerAnimation();
    }
}
