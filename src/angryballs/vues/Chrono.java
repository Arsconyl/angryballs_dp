package angryballs.vues;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/** Will be called at each blitter page */
public class Chrono implements ActionListener {

    Billard billard;
    // constructor that receives the GameCanvas that we will repaint every 60 milliseconds
    Chrono(Billard b) {
        this.billard = b;
    }
    // calls the method to repaint the anim
    public void actionPerformed(ActionEvent arg0) {
        billard.myRepaint();
    }

}