package angryballs.Visiteur;


public interface Visitable {
    public  void accept(Visiteur visiteur);
}
