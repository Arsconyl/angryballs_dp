package angryballs.Visiteur;


import angryballs.modele.Bille.Bille;

import java.awt.*;

public interface Visiteur {
    public void visite(Bille bille);
}
