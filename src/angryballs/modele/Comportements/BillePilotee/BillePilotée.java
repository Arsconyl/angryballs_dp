package angryballs.modele.Comportements.BillePilotee;

import angryballs.AnimationBilles;
import angryballs.modele.Bille.Bille;
import angryballs.modele.Bille.DecoratorComportementBille;
import angryballs.modele.Souris.Souris;

import java.util.Vector;

public class BillePilotée extends DecoratorComportementBille {

    public BillePilotée(Bille b) {
        super(b);
    }

    @Override
    public void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur, double hauteur) {
        this.bille.collisionContour(abscisseCoinHautGauche, abscisseCoinHautGauche, largeur, hauteur);
    }

    @Override
    public void gestionAccélération(Vector<Bille> billes) {
        this.bille.gestionAccélération(billes);
    }

    @Override
    public void gestionPilotage(AnimationBilles animationBilles) {
        this.bille.gestionPilotage(animationBilles);
        if(sourisDansBille(animationBilles.souris))
        {
            animationBilles.getEtat().Comportement(animationBilles);
        }
    }

    public boolean sourisDansBille(Souris souris)
    {
        int distanceCarree = (int)((souris.getNewPosSouris().x - this.getPosition().x)*(souris.getNewPosSouris().x - this.getPosition().x) + (souris.getNewPosSouris().y - this.getPosition().y)*(souris.getNewPosSouris().y - this.getPosition().y));
        return distanceCarree <= this.getRayon()*this.getRayon();
    }
}
