package angryballs.modele.Comportements.Accelerations;

import angryballs.AnimationBilles;
import angryballs.modele.Bille.Bille;
import angryballs.modele.Bille.DecoratorComportementBille;
import angryballs.modele.Souris.Souris;

import java.util.Vector;

public class AbscenceAcceleration extends DecoratorComportementBille {

    public AbscenceAcceleration(Bille b)
    {
        super(b);
    }

    @Override
    public void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur, double hauteur) {
        this.bille.collisionContour(abscisseCoinHautGauche, abscisseCoinHautGauche, largeur, hauteur);
    }

    @Override
    public void gestionAccélération(Vector<Bille> billes) {
        this.bille.gestionAccélération(billes);
    }

    @Override
    public void gestionPilotage(AnimationBilles animationBilles) {
        this.bille.gestionPilotage(animationBilles);
    }


}
