package angryballs.modele.Comportements.Collisions;

import angryballs.AnimationBilles;
import angryballs.modele.Souris.Souris;
import mesmaths.cinematique.Collisions;
import angryballs.modele.Bille.Bille;
import angryballs.modele.Bille.DecoratorComportementBille;

import java.util.Vector;

public class FranchissementParois extends DecoratorComportementBille{

    public FranchissementParois(Bille b)
    {
        super(b);
    }

    public void collisionContour(double abscisseCoinHautGauche,
                                 double ordonnéeCoinHautGauche, double largeur, double hauteur)
    {
        this.bille.collisionContour(abscisseCoinHautGauche, abscisseCoinHautGauche, largeur, hauteur);
        Collisions.collisionBilleContourPasseMuraille( bille.getPosition(), abscisseCoinHautGauche, ordonnéeCoinHautGauche, largeur, hauteur);

    }

    @Override
    public void gestionAccélération(Vector<Bille> billes) {
        this.bille.gestionAccélération(billes);
    }

    @Override
    public void gestionPilotage(AnimationBilles animationBilles) {
        this.bille.gestionPilotage(animationBilles);
    }

}
