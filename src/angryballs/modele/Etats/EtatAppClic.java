package angryballs.modele.Etats;

import angryballs.AnimationBilles;

public class EtatAppClic implements EtatApp{

    public void Comportement(AnimationBilles animationBilles)
    {
        animationBilles.lancerBille.lancer();
        animationBilles.lancerBille.arreter();
    }
}