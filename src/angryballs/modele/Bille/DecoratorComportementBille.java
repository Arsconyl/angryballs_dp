package angryballs.modele.Bille;

import angryballs.AnimationBilles;
import angryballs.modele.Souris.Souris;
import mesmaths.geometrie.base.Vecteur;

import java.awt.*;
import java.util.Vector;

public abstract class DecoratorComportementBille extends Bille{
    protected Bille bille;

    public DecoratorComportementBille(Bille b)
    {
        this.bille = b;
        this.getAccélération().set(Vecteur.VECTEURNUL);
    }

    @Override
    public Vecteur getPosition()
    {
        return this.bille.getPosition();
    }

    @Override
    public double getRayon() {
        return this.bille.getRayon();
    }

    @Override
    public Vecteur getVitesse() {
        return this.bille.getVitesse();
    }

    @Override
    public Vecteur getAccélération() {
        return this.bille.getAccélération();
    }

    @Override
    public int getClef() {
        return this.bille.getClef();
    }

    public Color getCouleur()
    {
        return this.bille.getCouleur();
    }


    public abstract void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur, double hauteur);

    public abstract void gestionAccélération(Vector<Bille> billes);

    public abstract void gestionPilotage(AnimationBilles animationBilles);

    @Override
    protected void setVitesse(Vecteur vitesse) {
        this.bille.setVitesse(vitesse);
    }

}
