package angryballs;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import angryballs.modele.Bille.Bille;
import angryballs.modele.Etats.EtatApp;
import angryballs.modele.Etats.EtatAppClic;
import angryballs.modele.Etats.EtatAppLibre;
import angryballs.modele.Lancer.CalculLancer;
import angryballs.modele.Sound.SoundPlayer;
import angryballs.modele.Souris.Souris;
import angryballs.vues.VueBillard;

/**
 * responsable de l'animation des billes, c-�-d responsable du mouvement de la liste des billes. met perp�tuellement � jour les billes.
 * g�re le d�lai entre 2 mises � jour (deltaT) et pr�vient la vue responsable du dessin des billes qu'il faut mettre � jour la sc�ne
 * 
 * ICI : IL N'Y A RIEN A CHANGER
 * */
public class AnimationBilles implements Runnable, MouseListener
{


    Vector<Bille> billes;   // la liste de toutes les billes en mouvement
    public Souris souris; //gestion de la souris
    VueBillard vueBillard;    // la vue responsable du dessin des billes
    private Thread thread;    // pour lancer et arr�ter les billes
    public CalculLancer lancerBille;
    SoundPlayer sf;
    public Bille billeCourante;
    private EtatApp etat;

    private static final double COEFF = 0.5;

    /**
     * @param billes
     * @param vueBillard
     */
    public AnimationBilles(Vector<Bille> billes, VueBillard vueBillard, Souris souris, SoundPlayer sf)
    {
        this.billes = billes;
        this.vueBillard = vueBillard;
        this.souris = souris;
        this.thread = null;     //est-ce utile �
        this.sf= sf;
        this.vueBillard.addMouseListener(this);
        this.etat = new EtatAppLibre();
        this.lancerBille = new CalculLancer(this);
    }

    @Override
    public void run()
    {
        try
        {
            double deltaT;  // d�lai entre 2 mises � jour de la liste des billes

            double minRayons = AnimationBilles.minRayons(billes);   //n�cessaire au calcul de deltaT
            double minRayons2 = minRayons*minRayons;                //n�cessaire au calcul de deltaT

            while (true)                        // gestion du mouvement
            {
                //deltaT = COEFF*minRayons2/(1+maxVitessesCarr�es(billes));       // mise � jour deltaT. L'addition + 1 est une astuce pour �viter les divisions par z�ro

                                                                                //System.err.println("deltaT = " + deltaT);
                deltaT = 10;

                int i;
                for ( i = 0; i < billes.size(); ++i)    // mise � jour de la liste des billes
                    {
                        billeCourante = billes.get(i);
                        billeCourante.d�placer(deltaT);                 // mise � jour position et vitesse de cette bille
                        billeCourante.gestionAcc�l�ration(billes);      // calcul de l'acc�l�ration subie par cette bille
                        if(billeCourante.gestionCollisionBilleBille(billes)){
                            sf.lancerSon();
                            sf.arreterSon();
                        }

                        billeCourante.gestionPilotage(this);
                        billeCourante.gestionCollisionBilleBille(billes);

                        billeCourante.collisionContour( 0, 0, vueBillard.largeurBillard(), vueBillard.hauteurBillard());        //System.err.println("billes = " + billes);
                    }

                //vueBillard.miseAJour();                                // on pr�vient la vue qu'il faut redessiner les billes


                Thread.sleep((int)deltaT);                          // deltaT peut �tre consid�r� comme le d�lai entre 2 flashes d'un stroboscope qui �clairerait la sc�ne
            }
        }

        catch (InterruptedException e)
        {
            /* arr�t normal, il n'y a rien � faire dans ce cas */
        }

    }

    /**
     * calcule le maximum de de la norme carr�e (pour faire moins de calcul) des vecteurs vitesse de la liste de billes
     *
     * */
    static double maxVitessesCarr�es(Vector<Bille> billes)
    {
        double vitesse2Max = 0;

        int i;
        double vitesse2Courante;

        for ( i = 0; i < billes.size(); ++i)
            if ( (vitesse2Courante = billes.get(i).getVitesse().normeCarr�e()) > vitesse2Max)
               vitesse2Max = vitesse2Courante;

        return vitesse2Max;
    }

    /**
     * calcule le minimum  des rayons de a liste des billes
     *
     *
     * */
    static double minRayons(Vector<Bille> billes)
    {
        double rayonMin, rayonCourant;

        rayonMin = Double.MAX_VALUE;

        int i;
        for ( i = 0; i < billes.size(); ++i)
            if ( ( rayonCourant = billes.get(i).getRayon()) < rayonMin)
               rayonMin = rayonCourant;

        return rayonMin;
    }


    public void lancerAnimation()
    {
        if (this.thread == null)
            {
                this.thread = new Thread(this);
                thread.start();
            }
    }

    public void arr�terAnimation()
    {
        if (thread != null)
            {
                this.thread.interrupt();
                this.thread = null;
            }
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        this.etat = new EtatAppClic();
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        this.etat = new EtatAppLibre();
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
    }

    public EtatApp getEtat() {
        return etat;
    }

    public void setEtat(EtatApp etat) {
        this.etat = etat;
    }
}
