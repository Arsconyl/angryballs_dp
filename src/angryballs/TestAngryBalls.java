package angryballs;

import java.awt.Color;
import java.util.Vector;

import angryballs.modele.Bille.Bille;
import angryballs.modele.Bille.BilleAvecComportement;
import angryballs.modele.Comportements.BillePilotee.BillePilotée;
import angryballs.modele.Sound.SoundPlayer;
import angryballs.modele.Souris.Souris;
import angryballs.vues.ObserveurEvent.EcouteurBoutonArreter;
import angryballs.vues.ObserveurEvent.EcouteurBoutonLancer;
import mesmaths.geometrie.base.Vecteur;
import angryballs.modele.Comportements.Accelerations.*;
import angryballs.modele.Comportements.Collisions.BloqueeParBord;
import angryballs.modele.Comportements.Collisions.FranchissementParois;
import angryballs.modele.Comportements.Collisions.Rebond;
import angryballs.vues.*;

//Coucou
/**
 * Gestion d'une liste de billes en mouvement ayant toutes un comportement différent
 * 
 * Idéal pour mettre en place le DP decorator
 * */
public class TestAngryBalls
{
    public static void main(String[] args)
    {

        //------------------- création de la liste (pour l'instant vide) des billes -----------------------

        Vector<Bille> billes = new Vector<>();

        Souris souris = new Souris();

        SoundPlayer sfCollision = new SoundPlayer("explosion.wav");
        SoundPlayer sfRebond = new SoundPlayer("punch.wav");

        //---------------- création de la vue responsable du dessin des billes -------------------------

        CadreAngryBalls cadre = new CadreAngryBalls("Angry balls",
                                                "Animation de billes ayant des comportements différents. Situation idéale pour mettre en place le DP Decorator",
                                                billes);
        cadre.getBillard().addMouseMotionListener(souris);
        cadre.montrer(); // on rend visible la vue
        cadre.getBillard().init();

        //------------- remplissage de la liste avec 4 billes -------------------------------



        double xMax, yMax;
        double vMax = 1;
        xMax = cadre.largeurBillard();      // abscisse maximal
        yMax = cadre.hauteurBillard();      // ordonnée maximale

        double rayon = 0.05*Math.min(xMax, yMax); // rayon des billes : ici toutes les billes ont le méme rayon, mais ce n'est pas obligatoire

        Vecteur p0, p1, p2, p3, p4, p5,  v0, v1, v2, v3, v4, v5;    // les positions des centres des billes et les vecteurs vitesse au démarrage.
                                                            // Elles vont étre choisies aléatoirement

        //------------------- création des vecteurs position des billes ---------------------------------

        p0 = Vecteur.créationAléatoire(0, 0, xMax, yMax);
        p1 = Vecteur.créationAléatoire(0, 0, xMax, yMax);
        p2 = Vecteur.créationAléatoire(0, 0, xMax, yMax);
        p3 = Vecteur.créationAléatoire(0, 0, xMax, yMax);
        p4 = Vecteur.créationAléatoire(0, 0, xMax, yMax);
        p5 = Vecteur.créationAléatoire(0, 0, xMax, yMax);

        //------------------- création des vecteurs vitesse des billes ---------------------------------

        v0 = Vecteur.créationAléatoire(-vMax, -vMax, vMax, vMax);
        v1 = Vecteur.créationAléatoire(-vMax, -vMax, vMax, 0);
        v2 = Vecteur.créationAléatoire(-vMax, -vMax, vMax, vMax);
        v3 = Vecteur.créationAléatoire(-vMax, -vMax, vMax, vMax);
        v4 = Vecteur.créationAléatoire(-vMax, -vMax, vMax, vMax);
        //v5 = new Vecteur(0.01, 0.01);
        v5 = Vecteur.créationAléatoire(-vMax, -vMax, vMax, vMax);
        //--------------- ici commence la partie é changer ---------------------------------

            Bille BilleMVTRebond = new BilleAvecComportement(p0, rayon, v0, Color.red);
            BilleMVTRebond = new AbscenceAcceleration(BilleMVTRebond);
            BilleMVTRebond = new Rebond(BilleMVTRebond, sfRebond);
            billes.add(BilleMVTRebond);

            Bille BilleMVTPesanteurFrottementRebond = new BilleAvecComportement(p1, rayon, v1, Color.yellow);
            BilleMVTPesanteurFrottementRebond = new AttractionVersBas(BilleMVTPesanteurFrottementRebond, new Vecteur(0,0.001));
            BilleMVTPesanteurFrottementRebond = new Freinage(BilleMVTPesanteurFrottementRebond);
            BilleMVTPesanteurFrottementRebond = new Rebond(BilleMVTPesanteurFrottementRebond, sfRebond);
            billes.add(BilleMVTPesanteurFrottementRebond);

            Bille BilleMVTNewtonFrottementRebond = new BilleAvecComportement(p2, rayon, v2, Color.green);
            BilleMVTNewtonFrottementRebond = new AttireeParBilles(BilleMVTNewtonFrottementRebond);
            BilleMVTNewtonFrottementRebond = new Freinage(BilleMVTNewtonFrottementRebond);
            BilleMVTNewtonFrottementRebond = new Rebond(BilleMVTNewtonFrottementRebond, sfRebond);
            billes.add(BilleMVTNewtonFrottementRebond);

            Bille BilleMvtRUPasseMurailles = new BilleAvecComportement(p3, rayon, v3, Color.cyan);
            BilleMvtRUPasseMurailles = new AbscenceAcceleration(BilleMvtRUPasseMurailles);
            BilleMvtRUPasseMurailles = new FranchissementParois(BilleMvtRUPasseMurailles);
            billes.add(BilleMvtRUPasseMurailles);

            Bille BilleMVTNewtonArret = new BilleAvecComportement(p4, rayon, v4, Color.black);
            BilleMVTNewtonArret = new AttireeParBilles(BilleMVTNewtonArret);
            BilleMVTNewtonArret = new BloqueeParBord(BilleMVTNewtonArret);
            billes.add(BilleMVTNewtonArret);

            Bille BillePilotee = new BilleAvecComportement(p5, rayon, v5, Color.orange);
            BillePilotee = new Rebond(BillePilotee, sfRebond);
            BillePilotee = new BillePilotée(BillePilotee);
            billes.add(BillePilotee);

        //---------------------- ici finit la partie é changer -------------------------------------------------------------


        //System.out.println("billes = " + billes);


        //-------------------- création de l'objet responsable de l'animation (c'est un thread séparé) -----------------------

        AnimationBilles animationBilles = new AnimationBilles(billes, cadre, souris, sfCollision);
                        animationBilles.lancerAnimation();

        //----------------------- mise en place des écouteurs de boutons qui permettent de contréler (un peu...) l'application -----------------

        EcouteurBoutonLancer EcouteurBoutonLancer = new EcouteurBoutonLancer(animationBilles);
        EcouteurBoutonArreter EcouteurBoutonArreter = new EcouteurBoutonArreter(animationBilles);

        //------------------------- activation des écouteurs des boutons et éa tourne tout seul ------------------------------

        //cadre.lancerBilles.addObservateur(new ObservateurLancer(), EcouteurBoutonLancer);
        //cadre.arreterBilles.addObservateur(new ObservateurArret(), EcouteurBoutonArreter);

        cadre.lancerBilles.addObservateur(EcouteurBoutonLancer);
        cadre.arreterBilles.addObservateur(EcouteurBoutonArreter);

    }

}
